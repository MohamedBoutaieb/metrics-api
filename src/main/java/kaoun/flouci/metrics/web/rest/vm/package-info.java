/**
 * View Models used by Spring MVC REST controllers.
 */
package kaoun.flouci.metrics.web.rest.vm;
